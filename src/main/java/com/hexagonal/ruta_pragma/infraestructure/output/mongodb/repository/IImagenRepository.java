package com.hexagonal.ruta_pragma.infraestructure.output.mongodb.repository;

import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.entity.ImagenEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IImagenRepository extends MongoRepository<ImagenEntity, String> {
}
