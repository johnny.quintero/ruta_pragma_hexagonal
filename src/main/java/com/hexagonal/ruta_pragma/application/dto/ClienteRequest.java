package com.hexagonal.ruta_pragma.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteRequest {
    private String tipoDocumento;
    private String numeroDocumento;
}
