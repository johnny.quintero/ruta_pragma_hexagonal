package com.hexagonal.ruta_pragma.domain.api;

import com.hexagonal.ruta_pragma.domain.model.Imagen;

import java.util.List;

public interface IImagenServicePort {

    Imagen saveImagen(Imagen imagen);
    List<Imagen> getAllImagenes();
    Imagen getImagen(String imagenId);
    void updateImagen(Imagen imagen);
    void deleteImagen(String imagenId);
}
