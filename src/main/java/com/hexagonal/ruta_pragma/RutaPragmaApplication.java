package com.hexagonal.ruta_pragma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RutaPragmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RutaPragmaApplication.class, args);
	}

}
