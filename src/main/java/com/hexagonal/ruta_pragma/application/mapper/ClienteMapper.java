package com.hexagonal.ruta_pragma.application.mapper;

import com.hexagonal.ruta_pragma.application.dto.ClienteDTO;
import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.domain.model.Imagen;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface ClienteMapper {

    Cliente toCliente(ClienteDTO clienteDTO);


    default ClienteDTO toClienteDTO(Cliente cliente, List<Imagen> imagenList){
        String imagen = imagenList.stream()
                .filter(img -> img.getId().equals(cliente.getImagenId())).findFirst().orElse(null).getImagen();

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setNombres(cliente.getNombres());
        clienteDTO.setApellidos(cliente.getApellidos());
        clienteDTO.setNumeroDocumento(cliente.getNumeroDocumento());
        clienteDTO.setTipoDocumento(cliente.getTipoDocumento());
        clienteDTO.setEdad(cliente.getEdad());
        clienteDTO.setCiudadNacimiento(cliente.getCiudadNacimiento());
        clienteDTO.setImagen(imagen);

        return clienteDTO;
    }

    @Mapping(source = "clienteDTO.imagen", target = "imagen")
    Imagen toImagen(ClienteDTO clienteDTO);

    default List<ClienteDTO> toResponseListClienteDTO(List<Cliente> clienteList, List<Imagen> imagenList){
        return clienteList.stream()
                .map(cliente -> {
                    ClienteDTO clienteDTO = new ClienteDTO();
                    clienteDTO.setNombres(cliente.getNombres());
                    clienteDTO.setApellidos(cliente.getApellidos());
                    clienteDTO.setNumeroDocumento(cliente.getNumeroDocumento());
                    clienteDTO.setTipoDocumento(cliente.getTipoDocumento());
                    clienteDTO.setEdad(cliente.getEdad());
                    clienteDTO.setCiudadNacimiento(cliente.getCiudadNacimiento());
                    clienteDTO.setImagen(imagenList.stream().filter(imagen -> {
                        return imagen.getId().equals(cliente.getImagenId());
                    }).findFirst().orElse(null).getImagen());
                    return clienteDTO;
                }).toList();
    }

}
