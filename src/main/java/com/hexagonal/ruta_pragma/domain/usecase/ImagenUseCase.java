package com.hexagonal.ruta_pragma.domain.usecase;

import com.hexagonal.ruta_pragma.domain.api.IImagenServicePort;
import com.hexagonal.ruta_pragma.domain.model.Imagen;
import com.hexagonal.ruta_pragma.domain.spi.IImagenPersistencePort;

import java.util.List;

public class ImagenUseCase implements IImagenServicePort {

    private final IImagenPersistencePort iImagenPersistencePort;

    public ImagenUseCase(IImagenPersistencePort iImagenPersistencePort){
        this.iImagenPersistencePort = iImagenPersistencePort;
    }

    @Override
    public Imagen saveImagen(Imagen imagen) {
        //llega nulo la imagen
        return this.iImagenPersistencePort.saveImagen(imagen);
    }

    @Override
    public List<Imagen> getAllImagenes() {
        return this.iImagenPersistencePort.getAllImagenes();
    }

    @Override
    public Imagen getImagen(String imagenId) {
        return this.iImagenPersistencePort.getImagen(imagenId);
    }

    @Override
    public void updateImagen(Imagen imagen) {
        this.iImagenPersistencePort.updateImagen(imagen);
    }

    @Override
    public void deleteImagen(String imagenId) {
        this.iImagenPersistencePort.deleteImagen(imagenId);
    }
}
