package com.hexagonal.ruta_pragma.infraestructure.exceptionhandler;

public enum ExceptionResponse {
    CLIENT_NOT_FOUND("Cliente no se encontro en la base de datos"),
    CLIENT_ALREADY_EXISTS("Ya existe un cliente registrado con el mismo tipo y numero de documento"),
    NO_DATA_FOUND("No se encontraron datos"),

    CLIENT_NOT_AGE_FOUND("No se encontraton clientes registrados con esa o mayor edad"),
    IMAGEN_NOT_FOUND("No se encontro foto asociada con los datos enviados");

    private String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
