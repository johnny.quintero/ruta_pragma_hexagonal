package com.hexagonal.ruta_pragma.infraestructure.exceptionhandler;

import com.hexagonal.ruta_pragma.infraestructure.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {
    private static final String MESSAGE = "Message";

    @ExceptionHandler(ClienteAlreadyExistsException.class)
    public ResponseEntity<Map<String, String>> handleClienteAlreadyExistsException(
            ClienteAlreadyExistsException clienteAlreadyExistsException) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.CLIENT_ALREADY_EXISTS.getMessage()));
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException noDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_DATA_FOUND.getMessage()));
    }

    @ExceptionHandler(ClienteNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleClienteNotFoundException(
            ClienteNotFoundException clienteNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.CLIENT_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(ImagenNotFoundException.class)
    public ResponseEntity<Map<String, String>> handlePhotoNotFoundException(
            ImagenNotFoundException imagenNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.IMAGEN_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(ClienteNotFoundAgeException.class)
    public ResponseEntity<Map<String, String>> handleClienteNotFoundAgeException(
            ClienteNotFoundAgeException clienteNotFoundAgeException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.CLIENT_NOT_AGE_FOUND.getMessage()));
    }
}
