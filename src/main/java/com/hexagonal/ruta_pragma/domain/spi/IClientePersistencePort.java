package com.hexagonal.ruta_pragma.domain.spi;

import com.hexagonal.ruta_pragma.domain.model.Cliente;

import java.util.List;

public interface IClientePersistencePort {

    Cliente saveCliente(Cliente cliente);
    List<Cliente> getAllClientes();
    Cliente getCliente(String tipoDocumento, String numeroDocumento);
    List<Cliente> getClienteAge(int edad);
    void updateCliente(Cliente cliente);
    void deleteCliente(String tipoDocumento, String numeroDocumento);
}
