package com.hexagonal.ruta_pragma.infraestructure.exceptions;



public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException(){
        super();
    }
}
