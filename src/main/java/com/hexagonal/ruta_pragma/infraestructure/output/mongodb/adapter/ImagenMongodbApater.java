package com.hexagonal.ruta_pragma.infraestructure.output.mongodb.adapter;

import com.hexagonal.ruta_pragma.domain.model.Imagen;
import com.hexagonal.ruta_pragma.domain.spi.IImagenPersistencePort;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.ImagenNotFoundException;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.NoDataFoundException;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.entity.ImagenEntity;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.mapper.IImagenEntityMapper;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.repository.IImagenRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class ImagenMongodbApater implements IImagenPersistencePort {
    private final IImagenRepository imagenRepository;
    private final IImagenEntityMapper imagenEntityMapper;


    @Override
    public Imagen saveImagen(Imagen imagen) {
        ImagenEntity imagenEntity = this.imagenRepository.save(this.imagenEntityMapper.toEntity(imagen));
        return this.imagenEntityMapper.toImagen(imagenEntity);
    }

    @Override
    public List<Imagen> getAllImagenes() {
        List<ImagenEntity> imagenEntityList = this.imagenRepository.findAll();
        if(imagenEntityList.isEmpty()){
            throw new NoDataFoundException();
        }
        return this.imagenEntityMapper.toImagenList(imagenEntityList);
    }

    @Override
    public Imagen getImagen(String imagenId) {
        return this.imagenEntityMapper.toImagen(this.imagenRepository.findById(imagenId).orElseThrow(ImagenNotFoundException::new));
    }

    @Override
    public void updateImagen(Imagen imagen) {
        this.imagenRepository.save(this.imagenEntityMapper.toEntity(imagen));
    }

    @Override
    public void deleteImagen(String imagenId) {
        this.imagenRepository.deleteById(imagenId);
    }
}
