package com.hexagonal.ruta_pragma.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO {
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombres;
    private String apellidos;
    private String ciudadNacimiento;
    private int edad;
    private String imagen;
}
