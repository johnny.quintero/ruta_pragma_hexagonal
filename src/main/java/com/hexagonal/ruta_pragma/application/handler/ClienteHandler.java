package com.hexagonal.ruta_pragma.application.handler;

import com.hexagonal.ruta_pragma.application.dto.ClienteDTO;
import com.hexagonal.ruta_pragma.application.dto.ClienteRequest;
import com.hexagonal.ruta_pragma.application.mapper.ClienteMapper;
import com.hexagonal.ruta_pragma.domain.api.IClienteServicePort;
import com.hexagonal.ruta_pragma.domain.api.IImagenServicePort;
import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.domain.model.Imagen;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class ClienteHandler implements IClienteHandler{

    private final IClienteServicePort iClienteServicePort;

    private final IImagenServicePort iImagenServicePort;

    private final ClienteMapper clienteMapper;


    @Override
    public void saveCliente(ClienteDTO clienteDTO) {
        Imagen imagenMappeada =this.clienteMapper.toImagen(clienteDTO);

        Imagen imagen = this.iImagenServicePort.saveImagen(imagenMappeada);

        Cliente cliente = this.clienteMapper.toCliente(clienteDTO);

        cliente.setImagenId(imagen.getId());

        this.iClienteServicePort.saveCliente(cliente);;
    }

    @Override
    public List<ClienteDTO> getAllClientes() {
        return this.clienteMapper.toResponseListClienteDTO(this.iClienteServicePort.getAllClientes(), this.iImagenServicePort.getAllImagenes());
    }

    @Override
    public ClienteDTO getCliente(ClienteRequest clienteRequest) {
        return this.clienteMapper.toClienteDTO(this.iClienteServicePort.getCliente(clienteRequest.getTipoDocumento(),clienteRequest.getNumeroDocumento()), this.iImagenServicePort.getAllImagenes());
    }

    @Override
    public List<ClienteDTO> getClienteAge(int edad) {
        List<Cliente> clienteList = this.iClienteServicePort.getClienteAge(edad);
        return this.clienteMapper.toResponseListClienteDTO(clienteList, this.iImagenServicePort.getAllImagenes());
    }

    @Override
    public void updateCliente(ClienteDTO clienteDTO) {
        Cliente oldCliente = this.iClienteServicePort.getCliente(clienteDTO.getTipoDocumento(), clienteDTO.getNumeroDocumento());
        Imagen newImagen = this.clienteMapper.toImagen(clienteDTO);
        newImagen.setId(oldCliente.getImagenId());
        this.iImagenServicePort.updateImagen(newImagen);
        Cliente newCliente = this.clienteMapper.toCliente(clienteDTO);
        newCliente.setId(oldCliente.getId());
        newCliente.setImagenId(oldCliente.getImagenId());
        this.iClienteServicePort.updateCliente(newCliente);
    }

    @Override
    public void deleCliente(ClienteRequest clienteRequest) {
        Cliente cliente = this.iClienteServicePort.getCliente(clienteRequest.getTipoDocumento(),clienteRequest.getNumeroDocumento());
        this.iClienteServicePort.deleteCliente(clienteRequest.getTipoDocumento(),clienteRequest.getNumeroDocumento());
        this.iImagenServicePort.deleteImagen(cliente.getImagenId());
    }
}
