package com.hexagonal.ruta_pragma.infraestructure.output.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cliente")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String numeroDocumento;
    private String tipoDocumento;
    private String nombres;
    private String apellidos;
    private int edad;
    private String ciudadNacimiento;
    private String imagenId;
}
