package com.hexagonal.ruta_pragma.infraestructure.output.jpa.adapter;

import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.domain.spi.IClientePersistencePort;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.ClienteAlreadyExistsException;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.ClienteNotFoundAgeException;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.ClienteNotFoundException;
import com.hexagonal.ruta_pragma.infraestructure.exceptions.NoDataFoundException;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.mapper.IClienteEntityMapper;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.repository.IClienteRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class ClienteJpaAdapter implements IClientePersistencePort {

    private final IClienteRepository iClienteRepository;
    private final IClienteEntityMapper iClienteEntityMapper;

    @Override
    public Cliente saveCliente(Cliente cliente) {
        if(this.iClienteRepository.getCliente(cliente.getTipoDocumento(), cliente.getNumeroDocumento()).isPresent()){
            throw new ClienteAlreadyExistsException();
        }
        return this.iClienteEntityMapper.toCliente(this.iClienteRepository.save(this.iClienteEntityMapper.toEntity(cliente)));
    }

    @Override
    public List<Cliente> getAllClientes() {
        List<Cliente> clienteList = this.iClienteEntityMapper.toClienteList(this.iClienteRepository.findAll());
        if(clienteList.isEmpty()){
            throw new NoDataFoundException();
        }
        return clienteList;
    }

    @Override
    public Cliente getCliente(String tipoDocumento, String numeroDocumento) {
        return this.iClienteEntityMapper.toCliente(this.iClienteRepository.getCliente(tipoDocumento, numeroDocumento).orElseThrow(ClienteNotFoundException::new));
    }

    @Override
    public List<Cliente> getClienteAge(int edad) {
        List<Cliente> clienteList = this.iClienteEntityMapper.toClienteList(this.iClienteRepository.getClienteAge(edad));
        if(clienteList.isEmpty()){
            throw new ClienteNotFoundAgeException();
        }
        return clienteList;
    }

    @Override
    public void updateCliente(Cliente cliente) {
        this.iClienteRepository.save(this.iClienteEntityMapper.toEntity(cliente));
    }

    @Override
    public void deleteCliente(String tipoDocumento, String numeroDocumento) {
        this.iClienteRepository.deleteCliente(tipoDocumento, numeroDocumento);
    }
}
