package com.hexagonal.ruta_pragma.application.handler;

import com.hexagonal.ruta_pragma.application.dto.ClienteDTO;
import com.hexagonal.ruta_pragma.application.dto.ClienteRequest;

import java.util.List;

public interface IClienteHandler {

    void saveCliente(ClienteDTO clienteDTO);

    List<ClienteDTO> getAllClientes();

    ClienteDTO getCliente(ClienteRequest clienteRequest);

    List<ClienteDTO> getClienteAge(int edad);

    void updateCliente(ClienteDTO clienteDTO);

    void deleCliente(ClienteRequest clienteRequest);

}
