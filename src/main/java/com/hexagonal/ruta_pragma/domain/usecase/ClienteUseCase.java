package com.hexagonal.ruta_pragma.domain.usecase;

import com.hexagonal.ruta_pragma.domain.api.IClienteServicePort;
import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.domain.spi.IClientePersistencePort;

import java.util.List;

public class ClienteUseCase implements IClienteServicePort {

    private final IClientePersistencePort iClientePersistencePort;

    public ClienteUseCase(IClientePersistencePort iClientePersistencePort){
        this.iClientePersistencePort = iClientePersistencePort;
    }

    @Override
    public Cliente saveCliente(Cliente cliente) {
        return this.iClientePersistencePort.saveCliente(cliente);
    }

    @Override
    public List<Cliente> getAllClientes() {
        return this.iClientePersistencePort.getAllClientes();
    }

    @Override
    public Cliente getCliente(String tipoDocumento, String numeroDocumento) {
        return this.iClientePersistencePort.getCliente(tipoDocumento, numeroDocumento);
    }

    @Override
    public List<Cliente> getClienteAge(int edad) {
        return this.iClientePersistencePort.getClienteAge(edad);
    }

    @Override
    public void updateCliente(Cliente cliente) {
        this.iClientePersistencePort.updateCliente(cliente);
    }

    @Override
    public void deleteCliente(String tipoDocumento, String numeroDocumento) {
        this.iClientePersistencePort.deleteCliente(tipoDocumento, numeroDocumento);
    }
}
