package com.hexagonal.ruta_pragma.infraestructure.exceptions;

public class ClienteAlreadyExistsException extends RuntimeException{
    public ClienteAlreadyExistsException(){
        super();
    }
}
