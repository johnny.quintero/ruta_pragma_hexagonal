package com.hexagonal.ruta_pragma.infraestructure.output.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "imagen")
@Data
public class ImagenEntity {
    @Id
    private String id;
    private String imagen;
}
