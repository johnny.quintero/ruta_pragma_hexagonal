package com.hexagonal.ruta_pragma.infraestructure.output.mongodb.mapper;

import com.hexagonal.ruta_pragma.domain.model.Imagen;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.entity.ImagenEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IImagenEntityMapper {
    ImagenEntity toEntity(Imagen imagen);

    Imagen toImagen(ImagenEntity imagenEntity);

    List<Imagen> toImagenList(List<ImagenEntity> imagenEntityList);
}
