package com.hexagonal.ruta_pragma.infraestructure.output.jpa.mapper;


import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.entity.ClienteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IClienteEntityMapper {

    ClienteEntity toEntity(Cliente cliente);

    Cliente toCliente(ClienteEntity clienteEntity);

    List<Cliente> toClienteList(List<ClienteEntity> clienteEntityList);
}
