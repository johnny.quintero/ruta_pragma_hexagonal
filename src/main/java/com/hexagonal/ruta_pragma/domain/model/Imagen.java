package com.hexagonal.ruta_pragma.domain.model;

public class Imagen {

    private String id;
    private String imagen;

    public Imagen() {
    }

    public Imagen(String id, String imagen) {
        this.id = id;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
