package com.hexagonal.ruta_pragma.infraestructure.exceptions;

public class ClienteNotFoundException extends RuntimeException{

    public ClienteNotFoundException(){
        super();
    }
}
