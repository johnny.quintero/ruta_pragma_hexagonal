package com.hexagonal.ruta_pragma.infraestructure.output.jpa.repository;

import com.hexagonal.ruta_pragma.domain.model.Cliente;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.entity.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface IClienteRepository extends JpaRepository<ClienteEntity, Integer> {
    @Transactional
    @Query(value = "SELECT * FROM cliente AS c WHERE c.tipo_documento = :tipoDocumento AND c.numero_documento = :numeroDocumento", nativeQuery = true)
    Optional<ClienteEntity> getCliente(@Param("tipoDocumento") String tipoDocumento, @Param("numeroDocumento") String numeroDocumento);

    @Transactional
    @Query(value = "SELECT * FROM cliente AS c WHERE c.edad >= :edad", nativeQuery = true)
    List<ClienteEntity> getClienteAge(@Param("edad") int edad);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM cliente WHERE (cliente.tipo_documento = :tipoDocumento AND cliente.numero_documento = :numeroDocumento)", nativeQuery = true)
    void deleteCliente(@Param("tipoDocumento") String tipoDocumento, @Param("numeroDocumento") String numeroDocumento);
}
