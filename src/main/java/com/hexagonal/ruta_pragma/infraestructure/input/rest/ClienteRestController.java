package com.hexagonal.ruta_pragma.infraestructure.input.rest;

import com.hexagonal.ruta_pragma.application.dto.ClienteDTO;
import com.hexagonal.ruta_pragma.application.dto.ClienteRequest;
import com.hexagonal.ruta_pragma.application.handler.ClienteHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cliente")
@RequiredArgsConstructor
public class ClienteRestController {

    private final ClienteHandler clienteHandler;

    @PostMapping("/")
    public ResponseEntity<Void> saveCliente(@RequestBody ClienteDTO clienteDTO){
        this.clienteHandler.saveCliente(clienteDTO);;
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/")
    public ResponseEntity<List<ClienteDTO>> getAllClientes(){
        return ResponseEntity.ok(this.clienteHandler.getAllClientes());
    }

    @GetMapping("/age/{age}")
    public ResponseEntity<List<ClienteDTO>> getClienteAge(@PathVariable(name = "age") int edad){
        return ResponseEntity.ok(this.clienteHandler.getClienteAge(edad));
    }

    @PostMapping("")
    public ResponseEntity<ClienteDTO> getCliente(@RequestBody ClienteRequest clienteRequest){
        return ResponseEntity.ok(this.clienteHandler.getCliente(clienteRequest));
    }

    @PutMapping("/")
    public ResponseEntity<Void> updateCliente(@RequestBody ClienteDTO clienteDTO){
        this.clienteHandler.updateCliente(clienteDTO);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/")
    public ResponseEntity<Void> deleteCliente(@RequestBody ClienteRequest clienteRequest){
        this.clienteHandler.deleCliente(clienteRequest);
        return ResponseEntity.noContent().build();
    }
}
