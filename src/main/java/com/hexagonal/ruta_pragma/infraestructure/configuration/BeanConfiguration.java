package com.hexagonal.ruta_pragma.infraestructure.configuration;

import com.hexagonal.ruta_pragma.domain.api.IClienteServicePort;
import com.hexagonal.ruta_pragma.domain.api.IImagenServicePort;
import com.hexagonal.ruta_pragma.domain.spi.IClientePersistencePort;
import com.hexagonal.ruta_pragma.domain.spi.IImagenPersistencePort;
import com.hexagonal.ruta_pragma.domain.usecase.ClienteUseCase;
import com.hexagonal.ruta_pragma.domain.usecase.ImagenUseCase;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.adapter.ClienteJpaAdapter;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.mapper.IClienteEntityMapper;
import com.hexagonal.ruta_pragma.infraestructure.output.jpa.repository.IClienteRepository;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.adapter.ImagenMongodbApater;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.mapper.IImagenEntityMapper;
import com.hexagonal.ruta_pragma.infraestructure.output.mongodb.repository.IImagenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final IClienteRepository iClienteRepository;
    private final IClienteEntityMapper clienteEntityMapper;

    private final IImagenRepository imagenRepository;
    private final IImagenEntityMapper imagenEntityMapper;

    @Bean
    public IClientePersistencePort clientePersistencePort() {
        return new ClienteJpaAdapter(this.iClienteRepository, this.clienteEntityMapper);
    }

    @Bean
    public IClienteServicePort clienteServicePort() {
        return new ClienteUseCase(clientePersistencePort());
    }

    @Bean
    public IImagenPersistencePort iImagenPersistencePort() {
        return new ImagenMongodbApater(this.imagenRepository, this.imagenEntityMapper);
    }

    @Bean
    public IImagenServicePort iImageServicePort() {
        return new ImagenUseCase(this.iImagenPersistencePort());
    }
}
